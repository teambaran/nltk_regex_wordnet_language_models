import argparse, re, nltk
from nltk import *
from nltk.corpus import wordnet as wn

sys.stdout = open('wordnet.txt','w')

word = "house"



#Number 1
synset_list = wn.synsets(word)
print("2.4.1 Solution:")
for synset in synset_list:
        print(synset, ':', synset.definition())#, ": ", synset.lemma_names())
print("")

selected_synset = synset_list[1]

#Number 2
hypo = selected_synset.hyponyms()
print("2.4.2 Solution:")
print("Hyponyms of: ", selected_synset, "are: ", hypo)
hypee = selected_synset.hypernyms()
print("Hypernyms of: ", selected_synset, "are: ", hypee)
hype = selected_synset.root_hypernyms()
print("Root Hypernyms of : ", selected_synset, "are: ", hype)
print("")

#Number 3
print("2.4.3 Solution")
print("Hypernym path from:", selected_synset, "to: ", hype, "is: ")
while selected_synset != selected_synset.root_hypernyms()[0]:
    print(selected_synset, "-> ", end='')
    selected_synset = selected_synset.hypernyms()[0]
print(selected_synset)


sys.stdout = sys.__stdout__